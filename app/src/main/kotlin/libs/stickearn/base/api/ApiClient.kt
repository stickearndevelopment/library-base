package libs.stickearn.base.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by angger on 2019-07-03.
 */

object ApiClient {

    private const val TIMEOUT_DEFAULT = 1
    private const val TIMEOUT_DOWNLOAD = 2

    fun <T> createService(serviceClass: Class<T>, baseUrl: String, TYPE_TIMEOUT: Int = TIMEOUT_DEFAULT): T {

        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val client = OkHttpClient.Builder()
        if (TYPE_TIMEOUT == TIMEOUT_DOWNLOAD){
            client.readTimeout(7, TimeUnit.MINUTES)
            client.writeTimeout(7, TimeUnit.MINUTES)
        } else {
            client.readTimeout(80000,TimeUnit.MILLISECONDS)
            client.writeTimeout(80000,TimeUnit.MILLISECONDS)
        }

        client.addInterceptor(interceptor)

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create(serviceClass)
    }
}

