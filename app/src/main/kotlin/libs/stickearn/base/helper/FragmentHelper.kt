package libs.stickearn.base.helper

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTransaction

/**
 * Created by angger on 2019-07-03.
 */

object FragmentHelper {

    fun replaceFragment(activity: FragmentActivity?, fragment: Fragment?, idContainer: Int, tag: String?) {
        activity?.supportFragmentManager?.beginTransaction()
                ?.replace(idContainer, fragment!!, tag)
                ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                ?.commit()
    }

    fun replaceFragment(activity: FragmentActivity?, fragment: Fragment?, idContainer: Int) {
        activity?.supportFragmentManager?.beginTransaction()
                ?.replace(idContainer, fragment!!)
                ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                ?.commitAllowingStateLoss()
    }

    fun addFragment(activity: FragmentActivity?, fragment: Fragment, idContainer: Int, backStack: String? = null) {
        activity?.supportFragmentManager?.beginTransaction()
                ?.add(idContainer, fragment)
                ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                ?.addToBackStack(backStack)
                ?.commit()
    }



    @SuppressLint("CommitTransaction")
    fun removeFragment(activity: FragmentActivity?, fragment: Fragment?){
        activity?.supportFragmentManager?.beginTransaction()?.remove(fragment!!)!!.commitAllowingStateLoss()
    }
}