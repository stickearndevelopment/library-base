package libs.stickearn.base.helper

import android.annotation.SuppressLint
import android.content.Context
import android.text.format.DateFormat
import libs.stickearn.base.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by angger on 2019-07-03.
 */

object DateHelper{
    private val tagClass = this::class.java.simpleName
    const val patternGlobal = "E, dd MMM yyyy kk:mm:ss"
    const val pattern: String = "yyyy-MM-dd HH:mm:ss"
    const val patternTime: String = "HH:mm"
    const val patternDate: String = "yyyy-MM-dd"
    const val api_format_date = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" // 2019-03-21T04:06:52.000Z "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    @SuppressLint("SimpleDateFormat")
    fun convertDateToLong(date: String, pattern:String): Long { // "dd-MM-yyyy HH:mm" 2018-07-05 21:09:09
        val f = SimpleDateFormat(pattern)
        return try {
            f.parse(date).time
        } catch (e: ParseException) {
            LogDebug.show(tagClass, e.message!!)
            0
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateNow(pattern: String): String {
        val dateFormat = SimpleDateFormat(pattern)
        return dateFormat.format(Date())
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateToString(date: Date): String{
        val dateFormat = SimpleDateFormat(pattern)
        return dateFormat.format(date)
    }

    @SuppressLint("NewApi")
    fun addDateLong(timeInMinutes: Int): Date {
        val now = Calendar.getInstance()
        now.add(Calendar.MINUTE, timeInMinutes)
        return now.time
    }

    @SuppressLint("SimpleDateFormat")
    fun convertSecondToTimeString(secondTime: Int): String {
        val dateLong = Date(secondTime * 1000L)
        val date = DateFormat.format(pattern, dateLong).toString()

        val formatter = SimpleDateFormat(pattern)
        val value = formatter.parse(date)

        val dateFormatter = SimpleDateFormat(pattern)
        dateFormatter.timeZone = TimeZone.getDefault()
        return dateFormatter.format(value)
    }

    @SuppressLint("SimpleDateFormat")
    fun generateDayFormat(context: Context): String {
        val dateFormat = SimpleDateFormat("EEEE, dd MMM YYYY")
        val result = dateFormat.format(Date())
        val english = context.resources.getStringArray(R.array.day_english)
        val local = context.resources.getStringArray(R.array.day_indonesian)
        val date = result.split(", ")
        return when (date[0]) {
            english[0] -> "${local[0]}, ${date[1]}"
            english[1] -> "${local[1]}, ${date[1]}"
            english[2] -> "${local[2]}, ${date[1]}"
            english[3] -> "${local[3]}, ${date[1]}"
            english[4] -> "${local[4]}, ${date[1]}"
            english[5] -> "${local[5]}, ${date[1]}"
            english[6] -> "${local[6]}, ${date[1]}"
            else -> result
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertStringDateToLong(date: String, pattern: String): Long { // "dd-MM-yyyy HH:mm"
        val f = SimpleDateFormat(pattern)
        return try {
            val d = f.parse(date)
            d.time
        } catch (e: ParseException) {
            e.printStackTrace()
            0
        }
    }

    fun isTimeToCheck(startTime: String, endTime:String): Boolean {
        val date = getDateNow("dd-MM-yyyy HH:mm")
        val dateNow = getDateNow("dd-MM-yyyy")
        val a = convertStringDateToLong("$dateNow $startTime", "dd-MM-yyyy HH:mm")
        val b = convertStringDateToLong("$dateNow $endTime", "dd-MM-yyyy HH:mm")
        val now = convertStringDateToLong(date, "dd-MM-yyyy HH:mm")
        return now in a until b
    }
}