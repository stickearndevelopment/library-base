package libs.stickearn.base.helper

import android.util.Log
import libs.stickearn.base.BuildConfig

/**
 * Created by angger on 06/07/18.
 */
 
object LogDebug{
    fun show(tag: String, message: String?) {
        if (BuildConfig.DEBUG) Log.e("Stickearn", "$tag: $message")
    }
}