package libs.stickearn.base.helper

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by angger on 2019-07-03.
 */

class RxBus {
    companion object {
        private val bus = PublishSubject.create<Any>()

        fun send(event: Any) { bus.onNext(event) }

        fun toObservable(): Observable<Any> { return bus }

        fun hasObservers(): Boolean { return bus.hasObservers() }

        fun <T> listen(eventType: Class<T>): Observable<T> = bus.ofType(eventType)
    }
}