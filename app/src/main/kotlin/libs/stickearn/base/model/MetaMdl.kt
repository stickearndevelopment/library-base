package libs.stickearn.base.model

import com.google.gson.annotations.SerializedName

/**
 * Created by angger on 2019-07-03.
 */

data class MetaMdl(
        @SerializedName("next_survey_at")
        val nextSurveyAt: String? = null
)