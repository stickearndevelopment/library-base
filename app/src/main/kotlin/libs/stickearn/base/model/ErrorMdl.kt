package libs.stickearn.base.model

import com.google.gson.annotations.SerializedName

/**
 * Created by angger on 2019-07-03.
 */

data class ErrorMdl(
        @SerializedName("http_code")
        val httpCode: Int = 0,
        @SerializedName("message")
        val message: String? = null,
        @SerializedName("status")
        val status: Boolean? = null,
        @SerializedName("meta")
        val meta: MetaMdl? = null
)