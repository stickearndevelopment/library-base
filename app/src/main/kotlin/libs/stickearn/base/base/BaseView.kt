package libs.stickearn.base.base

/**
 * Created by angger on 2019-07-03.
 */

interface BaseView{
    fun startLoading()
    fun stopLoading()
    fun errorLoading(errorMessage: String?)
}