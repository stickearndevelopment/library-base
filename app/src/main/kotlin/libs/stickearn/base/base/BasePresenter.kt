package libs.stickearn.base.base

import io.reactivex.disposables.Disposable

/**
 * Created by angger on 2019-07-03.
 */

abstract class BasePresenter<V>(var mView: V): CustomDisposable {
    private var compositeDisposable: MutableList<Disposable> = mutableListOf()

    override fun addCompositeDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun finishDisposable() {
        for (disposable in compositeDisposable){
            disposable.dispose()
        }
    }

    fun onDestroy() { finishDisposable() }
}