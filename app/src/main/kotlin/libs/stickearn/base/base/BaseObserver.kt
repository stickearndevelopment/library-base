package libs.stickearn.base.base

import android.text.TextUtils
import com.google.gson.Gson
import io.reactivex.observers.DisposableObserver
import libs.stickearn.base.helper.LogDebug
import libs.stickearn.base.model.ErrorMdl
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeoutException

/**
 * Created by angger on 2019-07-03.
 */

abstract class BaseObserver<T> : DisposableObserver<T>() {
    private val tagClass = this::class.java.simpleName

    override fun onComplete() { }

    override fun onNext(t: T) {
        onResponseSuccess(t)
    }

    override fun onError(e: Throwable) {
        LogDebug.show(tagClass, "error: ${e.message}")
        var message: String? = null
        when (e) {
            is HttpException -> {
                LogDebug.show(tagClass, "\n${e.code()}: ${e.message()}: ${e.response()}")
                message = e.message()
                when (val code = e.code()) {
                    400 -> {
                        val body = e.response().errorBody()
                        val gSon = Gson()
                        val adapter = gSon.getAdapter(ErrorMdl::class.java)
                        try {
                            val errorParser = adapter.fromJson(body!!.string())
                            val next = errorParser?.meta?.nextSurveyAt ?: e.message()
                            LogDebug.show(tagClass, "Error: $next")
                            message = next ?: e.message()
                        } catch (e: IOException) {
                            LogDebug.show(tagClass, "error: 400: ${e.message}")
                        }
                    }
                    403 -> message = e.message()
                    429 -> {}
                    500 -> {}
                    else -> {
                        LogDebug.show(tagClass, "code: $code, ${e.response().headers()}")
                        val res = e.response().errorBody().toString()
                        if (res.contains("message")) {
                            val errorBody = e.response().errorBody().toString()
                            val jObjError = JSONObject(errorBody)
                            LogDebug.show(tagClass, "HttpException: $jObjError")
                            message = try {
                                jObjError.getString("message")
                            } catch (e: Exception) {
                                LogDebug.show(tagClass, "HttpException: ${e.message}")
                                res
                            }
                        }
                    }
                }
            }

            is SocketTimeoutException -> message = "Request time out"

            is ConnectException -> {
                when {
                    e.message?.contains("failed")!! or e.message?.contains("Unable")!! -> {
                        message = "Please Check Your Connection"
                    }

                    e.message?.contains("HTTP")!! -> {
                        message = e.message.toString().substring(9)
                    }
                }
            }

            is TimeoutException -> message = "Connection Timeout"

            else -> {
                message = "Please check your internet connection"
                LogDebug.show("observer", "message: ${e.message}\n${e.printStackTrace()}")
            }
        }

        if (TextUtils.isEmpty(message)) message = "${e.message}"
        onResponseError(message!!)
    }

    abstract fun onResponseError(errorMessage: String)

    abstract fun onResponseSuccess(result: T)
}