package libs.stickearn.base.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import libs.stickearn.base.R
import libs.stickearn.base.helper.LogDebug
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.io.File

/**
 * Created by angger on 2019-07-03.
 */

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {
    private val tagClass = this::class.java.simpleName

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/GothamRnd-Light.otf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                )
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val code = event.keyCode
        if (code == KeyEvent.KEYCODE_VOLUME_DOWN) return true
        if (code == KeyEvent.KEYCODE_VOLUME_UP) return true
        return super.dispatchKeyEvent(event)
    }

    fun baseIntent(context: Context, target: Class<*>) {
        baseIntent(context, target, true)
    }

    fun baseIntent(context: Context, target: Class<*>, isFinish: Boolean) {
        try {
            val intent = Intent(context, target)
            startActivity(intent)
            if (isFinish) finish()
        } catch (e: Exception) {
            LogDebug.show(tagClass, "BaseIntent: ${e.message}")
            finish()
        }
    }

    fun baseToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun clearCacheData() {
        try {
            val cache = File(packageName, "cache")
            if (cache.exists()) cache.delete()
            LogDebug.show(tagClass, "clearCacheData: $packageName")
        } catch (e: PackageManager.NameNotFoundException) {
            LogDebug.show(tagClass, "clearCacheData ${e.message}")
        }
    }
}