package libs.stickearn.base.base

import io.reactivex.disposables.Disposable

/**
 * Created by angger on 2019-07-03.
 */
 
interface CustomDisposable {
    fun addCompositeDisposable(disposable: Disposable)
    fun finishDisposable()
}